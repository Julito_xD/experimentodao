import java.util.ArrayList;

import jakarta.xml.bind.annotation.*;

@XmlRootElement(name="libreria")
@XmlType(propOrder = {"nombre","libros"})
public class Libreria {
    private String nombre;
    private ArrayList<Libro> libros = new ArrayList();

    public Libreria() {
    }

    @XmlElement(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlElementWrapper(name="libros")
    @XmlElement(name="libro")
    public ArrayList<Libro> getLibros() {
        return libros;
    }

    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }
}