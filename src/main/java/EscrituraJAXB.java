import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import java.util.ArrayList;

public class EscrituraJAXB {
    public static void main(String[] args) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(Libreria.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            Libreria libreria = new Libreria();
            libreria.setNombre("La librería de Julito");

            ArrayList<Libro> libros = new ArrayList<>();
            Libro libro1 = new Libro();
                libro1.setIsbn("123");
                libro1.setTitulo("Java 8");
                libro1.setAutor("Herbert Schildt");
            Libro libro2 = new Libro();
                libro2.setIsbn("124");
                libro2.setTitulo("Curso de programación Web");
                libro2.setAutor("Scott Mccracken");
            Libro libro3 = new Libro();
                libro3.setIsbn("125");
                libro3.setTitulo("Phyton fácil");
                libro3.setAutor("Arnaldo Pérez Castaño");

            libros.add(libro1);
            libros.add(libro2);
            libros.add(libro3);
            libreria.setLibros(libros);

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            //jaxbMarshaller.marshal(libreria, new File("E:\\libreria.xml"));
            jaxbMarshaller.marshal(libreria, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
