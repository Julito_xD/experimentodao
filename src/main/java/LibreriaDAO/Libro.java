package LibreriaDAO;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

//EL TRANSFER CORRRESPONDE A LA CLASE NORMAL
@XmlRootElement(name="libro")
@XmlType(propOrder = {"id","name"})
public class Libro {
    private String name;
    private int id;

    public Libro() {
    }

    public Libro(String name, int id) {
        this.name = name;
        this.id = id;
    }
    @XmlElement(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @XmlAttribute(name="id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
