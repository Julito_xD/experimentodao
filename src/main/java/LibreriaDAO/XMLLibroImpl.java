package LibreriaDAO;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="libros")
public class XMLLibroImpl implements LibroDAO {
    List<Libro> libros;

    public XMLLibroImpl(){
        libros = new ArrayList<Libro>();
    }

    @Override
    public void updateLibro(Libro libro) {
        libros.add(libro);
        System.out.println("Libro: "+ libro.getName() + ", id " + libro.getId() + ", updated in the database");
    }

    @Override
    public void deleteLibro(Libro libro) {
        libros.remove(libro.getId());
        System.out.println("Libro: "+ libro.getName() + ", id " + libro.getId() + ", deleted from database");
    }

    @Override
    @XmlElement(name="libro")
    public List<Libro> getAllLibros() {
        return libros;
    }

    @Override
    public Libro getLibro(int id) {
        return libros.get(id);
    }
}