package LibreriaDAO;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import java.io.File;

public class LibroMain {
    public static void main(String[] args) {
        JAXBContext jaxbContextRead;
        JAXBContext jaxbContextWrite;
        try {
            jaxbContextRead = JAXBContext.newInstance(XMLLibroImpl.class);
            File file = new File("libreriaDao.xml");

            Unmarshaller jaxbUnmarshaller = jaxbContextRead.createUnmarshaller();
            XMLLibroImpl ldao = (XMLLibroImpl) jaxbUnmarshaller.unmarshal(file);

            System.out.println("BBDD:" + ldao + " correctamente cargardo");

             try {
                jaxbContextWrite = JAXBContext.newInstance(XMLLibroImpl.class);

                //¿Pará que sirve?
                Marshaller jaxbMarshaller = jaxbContextWrite.createMarshaller();

                Libro l1 = new Libro("Sinsajo",129);
             /* Libro l2 = new Libro("El perfume",127);
                Libro l3 = new Libro("El sabueso de los Basquerville",128);

                ldao.updateLibro(l1);
                ldao.updateLibro(l2);
                ldao.updateLibro(l3);*/

                 ldao.updateLibro(l1);

                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                jaxbMarshaller.marshal(ldao, new File("libreriaDAO.xml"));
                jaxbMarshaller.marshal(ldao, System.out);

                System.out.println();
                System.out.println(ldao.getAllLibros());

            } catch (JAXBException e) {
                e.printStackTrace();
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
