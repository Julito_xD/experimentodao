package LibreriaDAO;
import java.util.List;

public interface LibroDAO {
    public List<Libro> getAllLibros();
    public Libro getLibro(int id);
    public void updateLibro (Libro libro);
    public void deleteLibro (Libro libro);
}
